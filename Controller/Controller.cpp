#include "Controller.h"
#include <iostream>
void Controller::update() 
{
    
    for(auto x : ptr_v1_ ) {
        x->update();
    }
}   

void Controller::add(View* ptr_v)
{
    for ( auto x : ptr_v1_ )
    {
        if( ptr_v == x)
        {
            cout<<"view already exisits "<<endl;
            throw exception();
        }
    }
    ptr_v1_.push_front(ptr_v); 
}

void Controller::delete_view(View* ptr_v) {
    int check=1;
    int size=ptr_v1_.size();
    for(auto x : ptr_v1_) {
            if(x==ptr_v) {
                check=0;
                break;
            }
        }
        if(check){
            cout<<"Cannot deregister the view \n";
        }
        else {
            ptr_v1_.remove(ptr_v);
        }
}