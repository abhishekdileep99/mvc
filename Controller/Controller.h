#ifndef CONTROLLER_H
#define CONTROLLER_H
#include "../Model/Model.h"
#include "../View/View.h"
#include <list>
using namespace std;

class Controller : public Controller_base
{
	private:
		Model* ptr_m_;
		list<View*>  ptr_v1_;
	public:
		Controller(Model* ptr_m) : ptr_m_(ptr_m) { } 
		virtual void add(View* ptr_v);
		virtual void update();
		virtual void delete_view(View* ptr_v);
};

#endif