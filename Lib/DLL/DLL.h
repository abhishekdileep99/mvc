#ifndef DLL_H
#define DLL_H

class Node {
    public : 
        Node* left ; 
        Node* right;
        int val_;
};

class DLL 
{
    private : 
        Node *start ;
        Node *end;
    public : 
        void front();
        void back();
        void size();
          

};

#endif