#ifndef MODEL_H
#define MODEL_H
#include <bits/stdc++.h>
#include "../Controller/Controller_base.h"

class Model 
{
	public:
		virtual ~Model() { } 
		virtual void set_controller(Controller_base* ptr_c) = 0; 
		virtual void change(std::list<int>) = 0; 
		virtual std::list<int> get_x() = 0; 
};

#endif