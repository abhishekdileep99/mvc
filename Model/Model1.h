#ifndef MODEL1_H
#define MODEL1_H

#include "Model.h"

class Model1 : public Model
{
	private:
		Controller_base* ptr_c_;
		std::list<int> x_;
	public:
		Model1(std::list<int> x) : x_(x) {}
		virtual void set_controller(Controller_base* ptr_c); 
		void change(std::list<int> x);
		std::list<int> get_x(); 
};

#endif