#include "View_sum_digits.h"


void View_sum_digits::update()
{
    
    int x = ptr_model_->get_x().front();
    if ( x < 0 )
    {
        std::cout<<"x value is less than 0 ";
        throw std::exception() ;
    }
    int sum=0;
    while(x>0) {
        sum+=x%10;
        x=x/10;
    }
    std::cout << "Sum : " << sum << "\n";
}

void View_sum_digits::get_model(Model *ptr_model)
{
    ptr_model_ = ptr_model;
}

void View_sum_digits::register_view(Controller_base* ptr_c)
{
    ptr_c->add(this);
};

void View_sum_digits::deregister_view(Controller_base* ptr_c)
{
    ptr_c->delete_view(this);
};

