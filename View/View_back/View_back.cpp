#include "View_back.h"

void View_back::update()
{
    int x = ptr_model_->get_x().back();
    std::cout << "back : " << x << "\n";
}

void View_back::get_model(Model *ptr_model)
{
    ptr_model_ = ptr_model;
}

void View_back::register_view(Controller_base* ptr_c)
{
    ptr_c->add(this);
};

void View_back::deregister_view(Controller_base* ptr_c)
{
    ptr_c->delete_view(this);
};


