#include "View_front.h"

void View_front::update()
{
    int x = ptr_model_->get_x().front();
    std::cout << "Front  : " << x << "\n";
}

void View_front::get_model(Model *ptr_model)
{
    ptr_model_ = ptr_model;
}

void View_front::register_view(Controller_base* ptr_c)
{
    ptr_c->add(this);
};

void View_front::deregister_view(Controller_base* ptr_c)
{
    ptr_c->delete_view(this);
};


