#include "View_nBits.h"

void View_nBits::update()
{
    int x = ptr_model_->get_x().front();
    int sum=0;
    for (int i = 31; i >= 0; i--) {
        int k = x >> i;
        if (k & 1)
        {
            sum+=1;
        }
    }
    // while ( x > 0 )
    // {
    //     if(x %2 == 1 )
    //         sum += 1;
    //     x = x >> 1 ;
         
    // }
    std::cout << "No of 1 Bits :  " << sum << "\n";
}

void View_nBits::get_model(Model *ptr_model)
{
    ptr_model_ = ptr_model;
}

void View_nBits::register_view(Controller_base* ptr_c)
{
    ptr_c->add(this);
};

void View_nBits::deregister_view(Controller_base* ptr_c)
{
    ptr_c->delete_view(this);
};

