#include "View_sum.h"

void View_sum::update()
{
    std::list<int> x = ptr_model_->get_x();
    auto it = 0 ; 
    for( auto y : x )
    { 
        it += y;
    }
    std::cout << "Sum : " << it << "\n";
}

void View_sum::get_model(Model *ptr_model)
{
    ptr_model_ = ptr_model;
}

void View_sum::register_view(Controller_base* ptr_c)
{
    ptr_c->add(this);
};

void View_sum::deregister_view(Controller_base* ptr_c)
{
    ptr_c->delete_view(this);
};



