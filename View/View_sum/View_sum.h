#ifndef VIEW_SUM_H
#define VIEW_SUM_H
#include <iostream>
#include "../View/View.h"

class View_sum : public View
{
	private:
		Model *ptr_model_;
	public:
		virtual void update();
		virtual void get_model(Model *ptr_model);
		virtual void register_view(Controller_base* ptr_c);
		virtual void deregister_view(Controller_base* ptr_c);
};

#endif