//client program

#include <iostream>
#include <list>
using namespace std;

// includes the header modules
#include "Controller/Controller.h"
#include "Model/Model1.h"
#include "View/include_view.h"
int main()
{
	std::list<int> a { 1 , 2, 4 ,5 ,6 };
	Model1 m(a); 
	//Include default constructor
	Controller c(&m); 
	m.set_controller(&c); 

	// View_sq view_sq; 
	// view_sq.register_view(&c);
	// view_sq.get_model(&m);

	View_front vf;
	vf.register_view(&c);
	vf.get_model(&m);
	vf.update();

	View_back vb;
	View_count vc;
	View_sum vs;
	vb.register_view(&c);
	vc.register_view(&c);
	vs.register_view(&c);
	vb.get_model(&m);
	vc.get_model(&m);
	vs.get_model(&m);
	vb.update();
	vc.update();
	vs.update();


	// View_sum_digits view_sum;
	// //view_sum.register_view(&c);
	// //view_sum.get_model(&m);

	// View_nBits view_nbits;
	// view_nbits.register_view(&c);
	// view_nbits.get_model(&m);
	// a = { 3 ,4 ,5 ,6 };
	// m.change(a);
	// // view_nbits.deregister_view(&c);
	// cout<<endl;
	// a = { 7 , 5 ,7 };
	// m.change(a);
	                         
	// a = { -1};
	m.change({3 , 5 ,6});
	//View_back vb ; 
	//vb.register_view(&c);
	//vb.update();
	std::list<int> ls;
	
}